 
# 2017

## Crear data frame alteris para 2017=a1
```{r}
alter_1<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_01, 
                      alter_edad=r13_edad_01, 
                      alter_rel=r13_relacion_01,
                      alter_tiempo=r13_tiempo_01,
                      alter_barrio=r13_barrio_01, 
                      alter_educ=r13_educ_01, 
                      alter_relig=r13_relig_01, 
                      alter_ideol=r13_ideol_01)
#View(alter_1)
alter_2<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_02, 
                      alter_edad=r13_edad_02, 
                      alter_rel=r13_relacion_02,
                      alter_tiempo=r13_tiempo_02,
                      alter_barrio=r13_barrio_02, 
                      alter_educ=r13_educ_02, 
                      alter_relig=r13_relig_02, 
                      alter_ideol=r13_ideol_02)

alter_3<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_03, 
                      alter_edad=r13_edad_03, 
                      alter_rel=r13_relacion_03,
                      alter_tiempo=r13_tiempo_03,
                      alter_barrio=r13_barrio_03, 
                      alter_educ=r13_educ_03, 
                      alter_relig=r13_relig_03, 
                      alter_ideol=r13_ideol_03)

alter_4<- a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_04, 
                      alter_edad=r13_edad_04, 
                      alter_rel=r13_relacion_04,
                      alter_tiempo=r13_tiempo_04, 
                      alter_barrio=r13_barrio_04, 
                      alter_educ=r13_educ_04, 
                      alter_relig=r13_relig_04, 
                      alter_ideol=r13_ideol_04)

alter_5<-a %>%
        dplyr::select(.egoID, 
                      alter_sexo=r13_sexo_05, 
                      alter_edad=r13_edad_05, 
                      alter_rel=r13_relacion_05,
                      alter_tiempo=r13_tiempo_05, 
                      alter_barrio=r13_barrio_05, 
                      alter_educ=r13_educ_05, 
                      alter_relig=r13_relig_05, 
                      alter_ideol=r13_ideol_05)
```

## setear
```{r}
alter_1$n<-1
alter_2$n<-2
alter_3$n<-3
alter_4$n<-4
alter_5$n<-5
```

### Crear base long
```{r}
alteris<-rbind(alter_1,alter_2,alter_3,alter_4,alter_5)
alteris<-arrange(alteris, .egoID)
```

### Crear vector alter id
```{r}
alteris   <- rowid_to_column(alteris, var = ".altID")
alteris   <- as_tibble(alteris)
alteris$n <- NULL
```

### Recod alteris
```{r}
alteris$alter_educ <-factor(Recode(alteris$alter_educ,"1=1;2:3=2;4=3;5=4;-999=NA"))
alteris$alter_relig<-factor(Recode(alteris$alter_relig,"1=1;2=2;3=3;4=4;5=5;-999=NA"))
alteris$alter_ideol<-factor(Recode(alteris$alter_ideol,"1=1;2=2;3=3;4=4;5=5;6=6;-999=NA"))
alteris$alter_edad <-factor(Recode(alteris$alter_edad,"0:18=1;19:29=2;30:40=3;41:51=4;52:62=5;63:100=6"))
alteris$alter_sexo <-factor(Recode(alteris$alter_sexo,"1=1;2=2"))
#alteris<-na.omit(alteris)
```

### Data Frame Ego’s
```{r}
egos <-a %>%
       dplyr::select(.egoID, 
                     ego_sexo=m0_sexo, 
                     ego_edad=m0_edad, 
                     ego_ideol=c15, 
                     ego_educ=m01, 
                     ego_relig=m38, 
                     ego_ideol=c15)

egos <- as_tibble(egos)
```

### Recod data Ego's
```{r}
egos$ego_educ <-factor(Recode(egos$ego_educ,"1:3=1;4:5=2;6:7=3;8:10=4;-999:-888=NA"))
egos$ego_relig<-factor(Recode(egos$ego_relig,"1=1;2=2;9=3;7:8=4;3:6=5;-999:-888=NA"))
egos$ego_ideol<-factor(Recode(egos$ego_ideol,"9:10=1;6:8=2;5=3;2:4=4;0:1=5;11:12=6;-999:-888=NA"))
egos$ego_edad <-factor(Recode(egos$ego_edad,"18=1;19:29=2;30:40=3;41:51=4;52:62=5;63:100=6"))
egos$ego_sexo <-factor(Recode(egos$ego_sexo,"1=1;2=2"))
```

### join
```{r}
obs<-left_join(alteris,egos, by=".egoID")
obs$case<-1
```

### Descriptivos (Ejemplos)
```{r}
freq(obs$alter_sexo)
freq(obs$alter_educ)
freq(obs$ego_educ)
```

### Mean en tiempo * tipos de relación
```{r}
obs %>%
  summarise(
    mean.clo.esp   = mean(alter_tiempo[alter_rel=="1"], na.rm=T),
    mean.clo.hijo  = mean(alter_tiempo[alter_rel=="2"], na.rm=T),
    mean.clo.pari  = mean(alter_tiempo[alter_rel=="3"], na.rm=T),
    mean.clo.amig  = mean(alter_tiempo[alter_rel=="4"], na.rm=T),
    mean.clo.otro  = mean(alter_tiempo[alter_rel=="5"], na.rm=T),
    count.par.barr = sum((alter_rel=="3" & alter_barrio=="1"), na.rm=T))
```

### Crear un vector que sea TRUE siempre que alter tenga el mismo sexo que ego 
```{r}
#a$sexo_iden<-a$alter_sexo == a$ego_sexo
```

### otra alternativa para crear vector de identidad o "mismatch"
```{r}
obs$sexo_iden1 <-ifelse(obs$alter_sexo ==obs$ego_sexo,  0, 1) # mismatch
obs$edad_iden1 <-ifelse(obs$alter_edad ==obs$ego_edad,  0, 1) 
obs$educ_iden1 <-ifelse(obs$alter_educ ==obs$ego_educ,  0, 1) 
obs$ideol_iden1<-ifelse(obs$alter_ideol==obs$ego_ideol, 0, 1) 
obs$relig_iden1<-ifelse(obs$alter_relig==obs$ego_relig, 0, 1) 
```

